library(ggplot2)
library(reshape2)
library(plyr)

args <- commandArgs(trailingOnly = TRUE)


r <- read.table(args[1], header=T)
r <- rename(r, c("mu"="MTBF"))
rb <- ddply(r, seed + n + B + b + cores + speed + targetLoad ~ MTBF , transform, bufferRatio = buffer/buffer[alg=="static"])

p <- ggplot(rb[rb$alg %in% c("GreedyLP"),], aes(x=bufferRatio, y=maxstretch, color=factor(targetLoad))) + geom_smooth() + facet_wrap(~ MTBF, labeller=label_both, scales="free") + expand_limits(y=1)
p <- p + ylab("Maximum stretch") + xlab("Buffer size")
p <- p + scale_color_discrete(name="IO load")
ggsave(args[3], p, width=7, height=5, device=cairo_pdf)

p <- ggplot(rb[rb$alg %in% c("GreedyLP"),], aes(x=bufferRatio, y=avgstretch, color=factor(targetLoad))) + geom_smooth() + facet_wrap(~ MTBF, labeller=label_both, scales="free") + expand_limits(y=1)
p <- p + ylab("Average stretch") + xlab("Buffer size")
p <- p + scale_color_discrete(name="IO load")
ggsave(args[2], p, width=7, height=5, device=cairo_pdf)

rx <- rbind(rb, ddply(rb[rb$alg=="GreedyChk",], seed + n + B + b + cores + speed + targetLoad ~ MTBF , summarize, buffer = max(buffer)/3, bufferRatio=max(bufferRatio)/3, alg="maxChk", maxstretch=1, avgstretch=1))

print("Buffer ratio dynamic / static for different values of load and MTBF")
print(acast(ddply(rx[rx$alg == "Dnamic",], n+B+b+cores+speed+targetLoad~MTBF, summarize, buffer=mean(buffer), bufferRatio=round(1.0/mean(bufferRatio), digits=2)), targetLoad~MTBF, value.var="bufferRatio"))

