from instance import combinedPoints
import pulp
import sys


import logging
## logging.basicConfig(level = logging.DEBUG, stream=sys.stderr)


def solveLP(instance, alpha, save = False, static=True, progress=False):

    n = len(instance.apps)
    (allPoints, trueStart, trueEnd) = combinedPoints(instance)
    nbPoints = len(allPoints)
    
    start = [ max(trueStart[k]-10, 0) for k in range(n) ]
    end   = [ min(trueEnd[k]+10, len(allPoints)) for k in range(n) ]
    
    if progress:
        print("LP: Computed points", nbPoints, file=sys.stderr)

    
    def key(k, l):
        return (k, l)
    #        return str(k) + '-' + str(l)
    
    prob = pulp.LpProblem("Buffer Assignment")
    ###    B = pulp.LpVariable("B", 0, None)
    S = pulp.LpVariable("S", 0, None)
    Skl = pulp.LpVariable.dicts("S", [(k, l) for k in range(n) for l in range(start[k], end[k])], 0, None)
    Wkl = pulp.LpVariable.dicts("W", [(k, l) for k in range(n) for l in range(max(start[k]-1, 0), end[k])], 0, None)
    Rkl = pulp.LpVariable.dicts("R", [(k, l) for k in range(n) for l in range(max(start[k]-1, 0), end[k])], 0, None)
    for k in range(n):
        prob += (Wkl[k, max(start[k]-1, 0)] == 0)
        prob += (Rkl[k, max(start[k]-1, 0)] == 0)
    if static:
        Sk = pulp.LpVariable.dicts("Sexec", list(range(n)), 0, None)

    # Making sure that the freedom on S_kl values does not lead to absurd equivalent solutions
    # So we penalize the total sum with a small coefficient.
    prob += S + (0.01/(n*nbPoints)) * pulp.lpSum(Skl[(k, l)] for k in range(n) for l in range(nbPoints) if l >= start[k] and l < end[k])
    if static:
        for k in range(n):
            for l in range(trueStart[k], trueEnd[k]):
                prob += (Skl[(k, l)] == Sk[k])
    for l in range(nbPoints):
        prob += ( pulp.lpSum(Skl[(k, l)] for k in range(n) if l >= start[k] and l < end[k]) <= S)

    for l in range(1,nbPoints):
        written = pulp.lpSum(Wkl[(k, l)] - Wkl[(k, l-1)] for k in range(n) if l >= start[k] and l < end[k])
        readed = pulp.lpSum(Rkl[(k, l)] - Rkl[(k, l-1)] for k in range(n) if l >= start[k] and l < end[k] )
        prob += ( written <= instance.writeBW * (allPoints[l].time - allPoints[l-1].time) )
        prob += ( readed <= instance.readBW * (allPoints[l].time - allPoints[l-1].time) )
        if instance.totalBW < instance.writeBW + instance.readBW:
            prob += ( written + readed <= instance.totalBW * (allPoints[l].time - allPoints[l-1].time) )
        for k in range(n):
            if l >= start[k] and l < end[k]:
                prob += Wkl[(k, l)] >= Wkl[(k, l-1)]
                prob += Rkl[(k, l)] >= Rkl[(k, l-1)]

        
    for k in range(n):
        for l in range(start[k], end[k]):
            prob += ( Wkl[(k, l)] <= allPoints[l].writeAmount[k] )
            prob += ( Rkl[(k, l)] >= allPoints[l].readAmount[k] )
            prob += ( (allPoints[l].writeAmount[k] - Wkl[(k, l)]) + (Rkl[(k, l)] - allPoints[l].readAmount[k]) <= Skl[(k, l)])

    if progress:
        print("LP: Generated LP", file=sys.stderr)

    if save:
        prob.writeLP("model.lp")
        if progress:
            print("LP: Written LP", file=sys.stderr)


    try:
        solver = pulp.solvers.CPLEX_CMD(mip=False, msg=False)
        prob.solve(solver=solver)
    except pulp.solvers.PulpSolverError:
        prob.solve()
    if progress:
        print("LP: Solved LP", file=sys.stderr)
   
    Ws = [ [ Wkl[(k, l)].varValue if l >= start[k] and l < end[k] else 0 for l in range(nbPoints) ] for k in range(n) ]
    Rs = [ [ Rkl[(k, l)].varValue if l >= start[k] and l < end[k] else 0 for l in range(nbPoints) ] for k in range(n) ]
    Ss = [ [ Skl[(k, l)].varValue if l >= start[k] and l < end[k] else 0 for l in range(nbPoints) ] for k in range(n) ]
    return (S.varValue, Ws, Rs, Ss)
            
