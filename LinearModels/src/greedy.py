from instance import *
import copy

actionNone = 0
actionRead = 1
actionCompute = 2
actionWrite = 3
actionFinishing = 4

epsilon = 0.0001
#epsilon = 0
class Greedy:

    def __init__(self, out, verb=None):
        self.out = out
        self.verb = verb
    
    def run(self, instance, S, Svalues):
        apps = [ copy.copy(a) for a in instance.apps ]
        n = len(apps)
        i = 0
        for a in apps:
            a.S = Svalues[i]
            a.id = i
            i += 1
            a.action = actionNone
            a.dataInBuffer = 0
            ## allEvts = [ a.timePoints() for a in instance.apps ]
            a.readRate = 0
            a.writeRate = 0
            a.readRatePFS = 0
            a.writeRatePFS = 0
            a.totalWritten = 0
            a.totalRead = 0
            a.isRead = any(p.r for p in a.phases)
            a.isWrite = any(p.w for p in a.phases)
            # For now, only works for write only or read only apps
            assert(not (a.isRead and a.isWrite))
            assert(a.isRead or a.isWrite)
            
            a.nextTime = max(0, a.release - (10 if a.isRead else 0))
            a.phase = 0
            a.dateEndCompute = 0
            a.dataToRead = 0
            a.dataToWrite = 0
            a.finish = None
            
        now = 0
        step = 0
        while(True):
            lastNow = now
            app = min(apps, key=lambda a: a.nextTime)
            now = app.nextTime
            if now == math.inf:
                break

            if now > lastNow:
                # Update buffer state
                length = now - lastNow
                for a in apps:
                    dataRead = a.readRate * length
                    dataWritten = a.writeRate * length
                    dataReadPFS = a.readRatePFS * length
                    dataWrittenPFS = a.writeRatePFS * length
                    if self.verb:
                        print("Updating", a.id, dataRead, dataWritten, dataReadPFS, dataWrittenPFS, " -- ",
                              a.dataInBuffer, a.dataToRead, a.dataToWrite, file=sys.stderr)

                    a.dataInBuffer += dataReadPFS - dataWrittenPFS + dataWritten - dataRead
                    assert (a.dataInBuffer >= -epsilon and a.dataInBuffer <= a.S + epsilon)
                    a.dataToRead -= dataRead
                    a.dataToWrite -= dataWritten
                    a.totalRead += dataRead
                    a.totalWritten += dataWritten
                    assert(a.dataToWrite >= -epsilon and a.dataToRead >= -epsilon)


            # an app is write-active if:
            #  1/ it is currently in write phase 
            #  2/ OR it has write data in buffer
            # Same, app is read active if it is in read phase OR ..
            #  or what ? When do I prefetch ? Prefetch the next upcoming
            #  read phase if read BW and buffer space is available ?
            # I will first code without prefetching
            # Second option: code only for apps which only read or
            #  only write. Then I am read-active if in read phase OR
            #  buffer not full 
            
            # Possible app event: finish phase -- full buffer (read or
            #   write) -- empty buffer (read or write) -- done
            #   prefetching
            
            # Change phase if needed
            if not app.action:
                if app.isWrite:
                    app.action = actionCompute
                    app.dateEndCompute = now + app.phases[app.phase].c
                else:
                    app.action = actionCompute
                    # Simulate inactivity before start by
                    # computation. Allows apps to prefetch some time
                    # before starting.
                    app.dateEndCompute = app.release
                    
            elif app.action == actionCompute and now >= app.dateEndCompute:
                if app.isRead:
                    app.phase += 1
                    if app.phase < len(app.phases):
                        app.dataToRead = app.phases[app.phase].r
                        app.action = actionRead
                else:
                    app.dataToWrite = app.phases[app.phase].w
                    app.action = actionWrite
            elif (app.action == actionWrite and app.dataToWrite <= epsilon) or (app.action == actionRead and app.dataToRead <= epsilon):
                app.action = actionCompute
                app.readRate = 0
                app.writeRate = 0
                if app.isWrite:
                    app.phase += 1
                if app.phase < len(app.phases):
                    computeTime = app.phases[app.phase].c
                    app.dateEndCompute = now + computeTime
            if app.phase == len(app.phases):
                app.finish = now
                app.action = actionFinishing

            if self.verb:
                print("Now: ", now,  app.id, [a.nextTime for a in apps], file=sys.stderr)
            if self.out:
                for (idx,a) in enumerate(apps):
                    print(now, step, idx, "buffer", a.dataInBuffer, file=self.out)
                    print(now, step, idx, "totread", a.totalRead, file=self.out)
                    print(now, step, idx, "totwritten", a.totalWritten, file=self.out)
                    print(now, step, idx, "action", a.action, file=self.out)
                    print(now, step, idx, "toRead", a.dataToRead, file=self.out)
                    print(now, step, idx, "toWrite", a.dataToWrite, file=self.out)
                    print(now, step, idx, "writeRate", a.writeRatePFS, file=self.out)
                    print(now, step, idx, "readRate", a.readRatePFS, file=self.out)
            step += 1
                

            # Recomputing rates for all apps since one app
            # changing behavior may change the sharing of all others
            def isReadActive(a):
                return a.isRead and a.action and (a.action == actionRead or a.dataInBuffer < a.S - epsilon)
            def isWriteActive(a):
                return a.isWrite and (a.action == actionWrite or a.dataInBuffer > epsilon)
            readActiveApps = [a for a in apps if isReadActive(a) ]
            writeActiveApps = [a for a in apps if isWriteActive(a) ]

            for a in apps:
                a.readRatePFS = 0
                a.writeRatePFS = 0
                    
            stillActiveRead = readActiveApps.copy()
            stillActiveWrite = writeActiveApps.copy()
            readBW = instance.readBW
            totalBW = instance.totalBW
            writeBW = instance.writeBW
            while totalBW > epsilon and (readBW > epsilon or writeBW > epsilon) and (stillActiveWrite or stillActiveRead):
                if self.verb:
                    print("Read", totalBW, readBW, [a.id for a in stillActiveRead], file=sys.stderr)
                    print("Write", totalBW, writeBW, [a.id for a in stillActiveWrite], file=sys.stderr)
                targetRateTotal = totalBW / (len(stillActiveWrite) + len(stillActiveRead))
                if stillActiveRead: targetRateRead = min(readBW / len(stillActiveRead), targetRateTotal)
                if stillActiveWrite: targetRateWrite = min(writeBW / len(stillActiveWrite), targetRateTotal)
                for a in stillActiveRead:
                    if a.dataInBuffer >= a.S - epsilon and a.readBW < a.readRatePFS + targetRateRead:
                        a.readRatePFS = a.readBW
                        stillActiveRead.remove(a)
                    else: a.readRatePFS += targetRateRead
                for a in stillActiveWrite:
                    if a.dataInBuffer <= epsilon and a.writeBW < a.writeRatePFS + targetRateWrite:
                        a.writeRatePFS = a.writeBW
                        stillActiveWrite.remove(a)
                    else: a.writeRatePFS += targetRateWrite
                readBW = instance.readBW - sum(a.readRatePFS for a in readActiveApps)
                if readBW <= epsilon: stillActiveRead = []
                writeBW = instance.writeBW - sum(a.writeRatePFS for a in writeActiveApps)
                if writeBW <= epsilon: stillActiveWrite = []
                totalBW = instance.totalBW - sum(a.writeRatePFS for a in writeActiveApps) - sum(a.readRatePFS for a in readActiveApps)

                
            for a in readActiveApps:
                if a.action == actionRead:
                     if a.dataInBuffer <= epsilon: a.readRate = min(a.readBW, a.readRatePFS)
                     else: a.readRate = a.readBW
                else: a.readRate = 0
            for a in writeActiveApps:
                if a.action == actionWrite:
                    if a.dataInBuffer >= a.S - epsilon: a.writeRate = min(a.writeBW, a.writeRatePFS)
                    else: a.writeRate = a.writeBW
                else: a.writeRate = 0

            assert sum(a.writeRatePFS for a in apps) <= instance.writeBW + epsilon, "Used too much BW: %g > %g" % (sum(a.writeRatePFS for a in apps), instance.writeBW)
            assert all(a.writeRate <= a.writeBW + epsilon for a in apps), "One app (%d) exceeds its BW: %g > %g" % next((a.id, a.writeRate, a.writeBW) for a in apps if a.writeRate > a.writeBW)

            # Update next event times for all apps
            for a in apps:
                if a.action:
                    fillBufferRate = a.readRatePFS - a.readRate + a.writeRate - a.writeRatePFS
                    timeToBufferEvent = math.inf
                    if fillBufferRate > 0:
                        timeToBufferEvent = (a.S - a.dataInBuffer)/fillBufferRate
                    elif fillBufferRate < 0:
                        timeToBufferEvent = a.dataInBuffer/(-fillBufferRate)
                    else:
                        timeToBufferEvent = math.inf
                    timeToPhaseEvent = math.inf
                    if a.action == actionRead:
                        timeToPhaseEvent = a.dataToRead / a.readRate if a.readRate > 0 else math.inf
                    if a.action == actionWrite:
                        timeToPhaseEvent = a.dataToWrite / a.writeRate if a.writeRate > 0 else math.inf
                    if a.action == actionCompute:
                        timeToPhaseEvent = a.dateEndCompute - now
                    a.nextTime = now + min(timeToPhaseEvent, timeToBufferEvent)
                    if self.verb:
                        print("Active App", a.id, fillBufferRate, "(", a.readRatePFS, a.readRate, a.writeRate, a.writeRatePFS, ")", a.dataInBuffer, a.S, timeToBufferEvent, "--", timeToPhaseEvent, file=sys.stderr)
                    
                if self.verb:
                    print("App", a.id, a.action, a.dateEndCompute,
                          a.dataToRead, a.readRate, a.dataToWrite, a.writeRate, a.nextTime, file=sys.stderr)


        return [ app.finish for app in apps ]
