from collections import namedtuple
import random
import math
import sys

# compute is a time, read and write are data sizes
class Phase:
    def __init__(self, read, compute, write):
        self.r = read
        self.w = write
        self.c = compute

    def getTime(self, readBW, writeBW):
        return self.r / readBW + self.c + self.w / writeBW

EvtPoint = namedtuple("EvtPoint", "time readAmount writeAmount readRate writeRate")
        
class Application:
    def __init__(self, phases, readBW, writeBW, releaseTime = 0):
        self.phases = phases
        self.readBW = readBW
        self.writeBW = writeBW
        self.release = releaseTime


    # The following two assume that application bw is no more than platform bandwidth
    def flowTimeAlone(self):
        return sum(getTime(p, self.readBW, self.writeBW) for p in self.phases)

    def timePoints(self):
        res = []
        def interval(d, r, w, rr, wr):
            res.append(EvtPoint(d, r, w, rr, wr))
        for p in self.phases:
            interval(p.r / self.readBW, p.r, 0, self.readBW, 0)
            interval(p.c, 0, 0, 0, 0)
            interval(p.w / self.writeBW, 0, p.w, 0, self.writeBW)
        return res

    
def makeWriteApp(timeScale, writeProportion, writeBW, length, alpha = 0.25, release = 0):
    phases = []
    for i in range(length):
        x = 1+ (2 * random.random() - 1 ) * alpha
        w = writeProportion * timeScale * x * writeBW
        y = 1+ (2 * random.random() - 1 ) * alpha
        c = (1-writeProportion) * timeScale * y
        phases.append(Phase(0, c, w))
        
    return Application(phases, math.inf, writeBW, releaseTime = release)

def makeReadApp(timeScale, readProportion, readBW, length, alpha = 0.25, release = 0):
    phases = []
    for i in range(length):
        x = 1+ (2 * random.random() - 1 ) * alpha
        r = readProportion * timeScale * x * readBW
        y = 1+ (2 * random.random() - 1 ) * alpha
        c = (1-readProportion) * timeScale * y
        phases.append(Phase(r, c, 0))
        
    return Application(phases, readBW, math.inf, releaseTime = release)
        
        
class Instance:
    def __init__(self, readBW, writeBW, totalBW = None):
        self.apps = []
        self.readBW = readBW
        self.writeBW = writeBW
        if totalBW:
            self.totalBW = totalBW
        else:
            self.totalBW = readBW + writeBW
    def addApplication(self, app):
        self.apps.append(app)


def generateInstance(n):
    ins = Instance(1.5, 1.5, totalBW=2)

    for i in range(n):
        if random.choice([True, False]):
            ins.addApplication(makeWriteApp(5, 0.15, 5*(i+1)/n, 10, release = i))
        else:
            ins.addApplication(makeReadApp(5, 0.15, 5*(i+1)/n, 10, release = i))
    return ins



def combinedPoints(instance):
    n = len(instance.apps)

    # Need to add an "infinity" point to all applications
    allEvts = [ a.timePoints() for a in instance.apps ]
    times = [ a.release for a in instance.apps ]

    start = [-1] * n
    end = [-1] * n
    endTimes = [-1] * n
    readRates = [0] * n
    writeRates = [0] * n
    prevPoint = EvtPoint(0, [0] * n, [0]*n, [], [])
    points = [prevPoint]
    while True:
        # print(times)
        app = min(range(n), key=lambda k: times[k])
        #print(app, times[app])
        now = times[app]
        if now == math.inf:
            break
        if(now != prevPoint.time):
            point = EvtPoint(now,
                             [ prevPoint.readAmount[k] + readRates[k] * (now - prevPoint.time) for k in range(n) ],
                             [ prevPoint.writeAmount[k] + writeRates[k] * (now - prevPoint.time) for k in range(n) ],
                             [], [])
            points.append(point)
            prevPoint = point

        evt = None
        while not evt and allEvts[app]:
            evt = allEvts[app].pop(0)
            if not evt.time:
                evt = None
        if evt:
            if start[app] < 0:
                start[app] = len(points) - 1
                
            writeRates[app] = evt.writeRate
            readRates[app] = evt.readRate
            times[app] += evt.time
        else:
            end[app] = len(points)
            endTimes[app] = now
            writeRates[app] = 0
            readRates[app] = 0
            times[app] = math.inf


    ## print([endTimes[i] - instance.apps[i].release for i in range(n) ])
    return (points, start, end)


def saveToFile(filename, instance, points, written, readed, Svalues):
    out = open(filename, "w")
    n = len(instance.apps)
    totalW = [0] * n
    totalR = [0] * n
    for l in range(len(points)):
        p = points[l]
        for k in range(n):
            print(l, p.time, k, "solo", p.readAmount[k], p.writeAmount[k], file=out)
            print(l, p.time, k, "done", readed[k][l], written[k][l], file=out)
            print(l, p.time, k, "limit", p.readAmount[k] + Svalues[k][l], p.writeAmount[k] - Svalues[k][l], file=out)
        print(l, p.time, n, "done", sum(Svalues[k][l] for k in range(n)), sum(Svalues[k][l] for k in range(n)), file=out)

