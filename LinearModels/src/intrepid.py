import instance
import algLP
import greedy
import random
import itertools
import copy
import math
import sys

debug=False

class ApexApp:
    def __init__(self, nbCores, walltime, chkSize):
        self.cores = nbCores
        self.walltime = walltime
        self.chk = chkSize


##           (#kcores, walltime, chk size (GB), period,
apexApps = [ ApexApp(16,     262,      3200),
             ApexApp(4,      64,       2000),
             ApexApp(32,    128,      44800),
             ApexApp(30,    100,      3750) ]
             
rates = [0.646, 0.215, 0.081, 0.058]

def choices(seq, weights, k):
    s = sum(weights)
    weights = [ w / s for w in weights ]
    cumW = list(itertools.accumulate(weights))
    def oneChoice():
        x = random.random()
        i = next(i for (i, v) in enumerate(cumW) if x <= v)
        return copy.copy(seq[i])
    return [ oneChoice() for i in range(k) ]
        



def makeInstanceApp(app, alpha = 0.15):
    nPhases = int(math.ceil(app.walltime / app.period))
    return instance.makeWriteApp(app.period, app.chkTime / app.period, app.BW, nPhases, alpha, release = app.release)

def makeInstance(n, B, b, nbCores, speedFactor, targetLoad, mu):
    apps = choices(apexApps, weights=rates, k = n)

    # Conversions
    for a in apps:
        a.walltime *= 3600
        a.walltime /= speedFactor
        a.BW = b * a.cores
        a.chkTime = a.chk / a.BW
        a.period = math.sqrt(2 * a.chkTime * mu / (1000*a.cores))

    ## Schedule them.
    profile = [[0, nbCores]]
    for a in apps:
        #print("Prof:", profile)
        (i, (t, n)) = next((i, p) for (i, p) in enumerate(profile) if p[1] >= a.cores)
        #print("Index: ", i, t, n)
        a.release = t
        ## print("App:", a.cores, a.walltime, a.release, file = sys.stderr)
        end = t + a.walltime
        lastIndex = len(profile)
        for j in range(i, len(profile)):
            if(profile[j][0] >= end):
                lastIndex = j
                break
            profile[j][1] -= a.cores
        for j in range(i):
            profile[j][1] = min(profile[j][1], profile[i][1])
        profile.insert(lastIndex, [end, profile[lastIndex-1][1] + a.cores])
        #print("Prof After:", profile)

    # Compute load over time
    t = 0
    maxLoad = 0
    while True:
        try: 
            t = min(a.release for a in apps if a.release > t)
        except ValueError:
            break
        runningApps = [ a for a in apps if a.release <= t and a.release + a.walltime > t ]
        load = sum(a.chk / a.period for a in runningApps) / B
        maxLoad = max(load, maxLoad)
        ## print("After %g : load %g , size %g" % (t, load / B, buf), file=sys.stderr)

    sizeFactor = (targetLoad / maxLoad) ** 2

    print("Max Load %g, Target Load %g, sizeFactor %g" % (maxLoad, targetLoad, sizeFactor), file=sys.stderr)


    for a in apps:
        a.chk *= sizeFactor
        a.chkTime = a.chk / a.BW
        a.period = math.sqrt(2 * a.chkTime * mu / (1000*a.cores))

        
    t = 0
    maxLoad = 0
    while True:
        try: 
            t = min(a.release for a in apps if a.release > t)
        except ValueError:
            break
        runningApps = [ a for a in apps if a.release <= t and a.release + a.walltime > t ]
        load = sum(a.chk / a.period for a in runningApps) / B
        maxLoad = max(load, maxLoad)
        ## print("After %g : load %g , size %g" % (t, load / B, buf), file=sys.stderr)
    print("After: Max Load %g, Target Load %g, sizeFactor %g" % (maxLoad, targetLoad, sizeFactor), file=sys.stderr)

        
    ins = instance.Instance(10, B)
    for a in apps:
        ins.addApplication(makeInstanceApp(a))
    print("Nb phases:", [len(a.phases) for a in ins.apps], file = sys.stderr)
    return (ins, apps)

def stretchMaxAvg(optFlow, actualFinish, releases):
    stretches = [ (actualFinish[i] - releases[i]) / (optFlow[i]) for i in range(len(optFlow)) ]
    # print("Stretches", stretches)
    return (max(stretches), sum(stretches) / len(stretches))


def printHeader():
    print("seed n B b cores speed targetLoad mu alg buffer maxstretch avgstretch")

def doOneRun(seed, n, B, b, nbCores, speedFactor, targetLoad, mu, dont=False):
    def printInfo(alg, buffersize, maxStretch, avgStretch):
        print(seed, n, B, b, nbCores, speedFactor, targetLoad, mu, alg, buffersize, maxStretch, avgStretch)

    (i, apps) = makeInstance(n, B, b, nbCores, speedFactor, targetLoad, mu * 365 * 24 * 3600)

    (allPoints, start, end) = instance.combinedPoints(i)
    flowTimeMin = [ sum(p.getTime(a.readBW, a.writeBW) for p in a.phases) for a in i.apps]
    flowTimeAlone  = [ sum(p.getTime(i.readBW, i.writeBW) for p in a.phases) for a in i.apps]
    ## print("Stretch", [flowTimeAlone[i] / flowTimeMin[i] for i in range(n)], file=sys.stderr)


    def buffersize(app):
        tmpIns = instance.Instance(i.readBW, i.writeBW)
        tmpIns.addApplication(app)
        (S, w, r, Sv) = algLP.solveLP(tmpIns, 0.5, static=True, save=False)
        return S
    for (idx, a) in enumerate(i.apps):
        if not dont: 
            apps[idx].indivBufferSize = buffersize(a)
        else:
            apps[idx].indivBufferSize = 0
            
            
    # Compute load over time
    t = 0
    maxTotalIndivBufSize = 0
    maxLoad = 0
    maxChkSize = 0
    while True:
        try: 
            t = min(a.release for a in apps if a.release > t)
        except ValueError:
            break
        runningApps = [ a for a in apps if a.release <= t and a.release + a.walltime > t ]
        load = sum(a.chk / a.period for a in runningApps) / B
        buf = sum(a.indivBufferSize for a in runningApps)
        chkSize = sum(a.chk for a in runningApps)
        maxTotalIndivBufSize = max(buf, maxTotalIndivBufSize)
        maxLoad = max(load, maxLoad)
        maxChkSize = max(chkSize, maxChkSize)
        ## print("After %g : load %g , size %g" % (t, load / B, buf), file=sys.stderr)

    printInfo("maxLoad", maxLoad, 1, 1)
    printInfo("maxS0", maxTotalIndivBufSize, 1, 1)

    if dont:
        return

    (Sstar, write, read, Svalues) = algLP.solveLP(i, 0.5, static=True, save=False)
    printInfo("static", Sstar, 1, 1)
    if debug:
        instance.saveToFile("intrepid.tst.stat.dat", i, allPoints, write, read, Svalues)

    prop = [max(s)/Sstar for s in Svalues]
    
    if debug:
        g = greedy.Greedy(out=open("intrepid.tst.greedy.dat", "w"))
    else:
        g = greedy.Greedy(out=None)
        
    bufSize = 1 * Sstar
    finishTimes = g.run(i, bufSize, [p * bufSize for p in prop]) 
    (maxS, avgS) = stretchMaxAvg(flowTimeMin, finishTimes, [a.release for a in apps])
    printInfo("GreedyLP", bufSize, maxS, avgS)

    g.out = None
    for s in [0, 0.1, 0.25, 0.5, 0.75, 1.25, 1.5, 2, 3]:
        bufSize = s * Sstar
        finishTimes = g.run(i, bufSize, [p * bufSize for p in prop]) 
        (maxS, avgS) = stretchMaxAvg(flowTimeMin, finishTimes, [a.release for a in apps])
        printInfo("GreedyLP", bufSize, maxS, avgS)

    for s in [1, 0, 0.1, 0.25, 0.5, 0.75, 1.25, 1.5, 2, 3]:
        bufSize = s * maxChkSize
        finishTimes = g.run(i, bufSize, [s * a.chk for a in apps]) 
        (maxS, avgS) = stretchMaxAvg(flowTimeMin, finishTimes, [a.release for a in apps])
        printInfo("GreedyChk", bufSize, maxS, avgS)

    for s in [1, 0, 0.1, 0.25, 0.5, 0.75, 1.25, 1.5, 2, 3]:
        bufSize = s * maxTotalIndivBufSize
        finishTimes = g.run(i, bufSize, [s * a.indivBufferSize for a in apps]) 
        (maxS, avgS) = stretchMaxAvg(flowTimeMin, finishTimes, [a.release for a in apps])
        printInfo("GreedyIndiv", bufSize, maxS, avgS)


        
    (SstarDyn, writeDyn, readDyn, SvaluesDyn) = algLP.solveLP(i, 0.5, static=False, save=False)
    printInfo("Dnamic", SstarDyn, 1, 1)
    if debug:
        instance.saveToFile("intrepid.tst.dyn.dat", i, allPoints, write, read, Svalues)


if __name__ == "__main__":
    # random.seed(54687)

    if False:
        random.seed(4215119439)
        doOneRun(4215119439, 30, 160, 20, 96, 10, 10, 5)
        exit(0)

    random.seed(12098)

    seeds = [ random.getrandbits(32) for k in range(9) ]
    
    ## B = 160 #GB/s
    ## b = 20  #GB/s/kcore
    ## mu = 50 * 365 * 24 * 3600
    #mu = 1.3e10 # 408 years

    printHeader()
    for s in seeds:
        random.seed(s)
        for mtbf in [5, 10, 25, 50]:

            doOneRun(s, 30, 160, 20, 96, 20, 0.2, mtbf)
            doOneRun(s, 30, 160, 20, 96, 15, 0.5, mtbf)
            doOneRun(s, 30, 160, 20, 96, 5, 0.8, mtbf)
    

    
    
    
