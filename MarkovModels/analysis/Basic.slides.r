library(ggplot2)
library(plyr)
library(reshape2)

args <- commandArgs(trailingOnly = TRUE)

t <- read.table(args[1], header=T)

p <- ggplot(t[t$seed == 1258 & t$pmax < 1,], aes(x=S, y = idle, color=factor(alpha))) + geom_line() + facet_wrap(nrow=2, ~pmax, scales="free", labeller="label_both") + expand_limits(y=0)
p <- p + xlab("Burst Buffer size")
p <- p + ylab("Proportion of idle time")
p <- p + theme(legend.position = "bottom")
p <- p + scale_color_discrete(name = "alpha", guide=guide_legend(nrow=1))

ggsave(args[2], p, width=5, height=5,device=cairo_pdf)
