library(ggplot2)
library(reshape2)
library(plyr)

args <- commandArgs(trailingOnly = TRUE)

t <- read.table(args[1], header=T)

tfiltered <- t[! (t$AdHoc > 0.999 & t$ChernoffUB > 0.999 & t$ChernoffLB > 0.999),]

tsumm <- ddply(tfiltered, ~X+pmax+alpha, summarize, max = max(AdHoc), mean=mean(AdHoc), median=median(AdHoc))

ut <- melt(tsumm, id=c("X", "pmax", "alpha"))

p <- ggplot(ut, aes(x=X, y=value, color=variable)) + geom_line() + facet_wrap(~pmax, scales="free_x")
p <- p + ylab("Cumulative Distribution Function") + xlab("InstantLoad")
p <- p + theme(legend.position="bottom")
# p <- p + scale_color_discrete("Type", breaks=c("AdHoc", "ChernoffUB", "ChernoffLB"), labels=c("Ad Hoc", "Chernoff Upper Bound", "Chernoff Lower Bound"))

ggsave("plots/AdHocVariability.pdf", p, width=12, height=8,device=cairo_pdf)

tall <- melt(tfiltered, id=c("X", "pmax", "alpha", "seed", "B"))

p <- ggplot(tall, aes(x=X, y=value, color=variable)) + geom_line() + facet_grid(seed~pmax, scales="free")
p <- p + ylab("Cumulative Distribution Function") + xlab("InstantLoad")
p <- p + theme(legend.position="bottom")
p <- p + scale_color_discrete("Type", breaks=c("AdHoc", "ChernoffUB", "ChernoffLB"), labels=c("Ad Hoc", "Chernoff Upper Bound", "Chernoff Lower Bound"))

ggsave("plots/ChernoffAll.pdf", p, width=12, height=8,device=cairo_pdf)

p <- ggplot(tall[tall$seed == 2345 & tall$pmax %in% c(0.01, 0.05, 0.1, 0.2, 0.3, 0.5),], aes(x=X, y=value, color=variable)) + geom_line() + facet_wrap(~pmax, scales="free", labeller="label_both")
p <- p + ylab("Cumulative Distribution Function") + xlab("InstantLoad")
p <- p + theme(legend.position="bottom")
p <- p + scale_color_discrete("Type", breaks=c("AdHoc", "ChernoffUB", "ChernoffLB"), labels=c("Ad Hoc", "Chernoff Upper Bound", "Chernoff Lower Bound"))

ggsave(args[2], p, width=6, height=4,device=cairo_pdf)


# ks <-  c(1, 1.5, 2, 3.5, 3, 3.5, 4, 4.5, 5, 6, 7, 8, 9, 10)

# t <- ddply(t, ~X+pmax+alpha, summarize, max = max(AdHoc), mean=mean(AdHoc), med=median(AdHoc), min=min(AdHoc))

# p <- ggplot(t[(t$X/100) %in% ks,], aes(x=pmax,y=1-min,color=factor(X/100)))
# p <- p + geom_line() +scale_y_log10()
# p <- p + ylab("Pr(X > kB)") + scale_color_discrete(name="k")
# ggsave("Distrib-BwBB.pdf", p, width=5, height=5)

