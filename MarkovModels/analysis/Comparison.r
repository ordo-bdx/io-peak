library(ggplot2)
library(plyr)

args <- commandArgs(trailingOnly = TRUE)

t <- read.table(args[1], header=TRUE)

## col.names = c("pmax", "alpha", "S", "ratiomax", "nb", "res"))

tChern <- t[t$runNB == -1,]

nbRows <- nrow(tChern)
varValues <- unique(t$var)
varValues <- varValues[!is.na(varValues)]
tChern <- tChern[rep(seq(nbRows), length(varValues)),]
tChern$var <- rep(varValues, rep(nbRows, length(varValues)))

tSim <- ddply(t[t$runNB >= 0,], ~ pmax + alpha + S + var, summarize,
     resmin = min(idle), resmax = max(idle)) 

p <- ggplot(tChern, aes(x=S, y = idle, color = factor(alpha)))
p <- p + geom_line(linetype="dashed")
p <- p + geom_ribbon(data = tSim, aes(x=S, ymin = resmin, ymax=resmax, color=factor(alpha), fill=factor(alpha)),alpha=0.5, inherit.aes=FALSE)
p <- p + facet_grid(var~pmax, scales="free", labeller=label_both)

p <- p + xlab("S") + ylab("Proportion of Waste")
p <- p + scale_color_discrete("alpha") + scale_fill_discrete("alpha")

ggsave(args[2], p, width=10, height=5.3, device=cairo_pdf)