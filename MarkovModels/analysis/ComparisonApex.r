library(ggplot2)
library(plyr)

args <- commandArgs(trailingOnly = TRUE)

t <- read.table(args[1], header=TRUE)

tChern <- t[t$runNB == -1,]

tSim <- ddply(t[t$runNB >= 0,], ~ alpha + S, summarize,
     resmin = min(idle), resmax = max(idle)) 

p <- ggplot(tChern, aes(x=S, y = idle, color = factor(alpha)))
p <- p + geom_line(linetype="dashed")
p <- p + geom_ribbon(data = tSim, aes(x=S, ymin = resmin, ymax=resmax, color=factor(alpha), fill=factor(alpha)),alpha=0.5, inherit.aes=FALSE)

p <- p + xlab("S") + ylab("Proportion of Waste")
alphas <- unique(t$alpha)
alphaLabels <- alphas
alphaLabels[alphaLabels < 0] <- "Original"
p <- p + scale_color_discrete("alpha", breaks=alphas, labels=alphaLabels)
p <- p + scale_fill_discrete("alpha", breaks=alphas, labels=alphaLabels)
p <- p + theme(legend.position="bottom")

ggsave(args[2], p, width=5, height=3.5, device=cairo_pdf)