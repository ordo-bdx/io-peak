library(ggplot2)
library(reshape2)
library(plyr)

args <- commandArgs(trailingOnly = TRUE)

t <- read.table(args[1], header = T)
# col.names=c("seed", "pmax", "alpha", "S", "threshold", "thresholdRatio", "waste", "keep", "wastePrecise"))

t[t$thresholdRatio == -1, "thresholdRatio"] <- 0

prep_plot <- function(p) {
    p <- p + xlab("Burst buffer size")
    p <- p + scale_color_discrete(name="Threshold Ratio", guide=guide_legend(nrow=1))
    p <- p + facet_grid(pmax~alpha, scales="free", labeller=label_both) + expand_limits(y=0)
    p <- p + theme(legend.position = "bottom")
    return( p)
    }

p <- ggplot(t[t$thresholdRatio != 0.05,], aes(x=S, y=idle, color=factor(thresholdRatio))) + geom_line()
p <- p + geom_point(data=t[t$thresholdRatio == 0,], color="black", size=0.5)
p <- p + ylab("Proportion of idle time")
p <- prep_plot(p)

ggsave(args[2], p, width=5, height = 5,device=cairo_pdf)

q <- ggplot(t[t$thresholdRatio != 0.05,], aes(x=S, y=keepProb, color=factor(thresholdRatio))) + geom_line() 
q <- q + geom_point(data=t[t$thresholdRatio == 0,], color="black", size=0.5)
q <- q + ylab("Proportion of time without emptying the BB")
q <- prep_plot(q)

ggsave(args[3], q, width=5, height=5,device=cairo_pdf)


