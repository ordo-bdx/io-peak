library(ggplot2)
library(reshape2)
library(plyr)

args <- commandArgs(trailingOnly = TRUE)

t <- read.table(args[1], header = T)
# col.names=c("seed", "pmax", "alpha", "S", "threshold", "thresholdRatio", "waste", "keep", "wastePrecise"))

t[t$thresholdRatio == -1, "thresholdRatio"] <- 0

t<- melt(t, measure.vars=c("idle", "keepProb"), variable.name="Time")
t$Time <- factor(t$Time, levels=c("idle", "keepProb"), labels=c("Idle", "Quiet"))

prep_plot <- function(p) {
    p <- p + xlab("Burst buffer size")
    p <- p + scale_color_discrete(name="Threshold Ratio", guide=guide_legend(nrow=1))
    p <- p + facet_grid(Time~alpha, scales="free", labeller=label_both)
    p <- p + theme(legend.position = "bottom")
    return( p)
    }

p <- ggplot(t[t$thresholdRatio != 0.05 & t$pmax == 0.1,], aes(x=S, y=value, color=factor(thresholdRatio))) + geom_line()
p <- p + geom_point(data=t[t$thresholdRatio == 0 & t$pmax==0.1,], color="black", size=0.5)
p <- p + ylab("")
p <- prep_plot(p)

ggsave(args[2], p, width=6, height = 4,device=cairo_pdf)


