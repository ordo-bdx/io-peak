#!/usr/bin/env python3

import argparse
import models
import random

parser = argparse.ArgumentParser(description = "Markov Chain model for Burst-Buffer : Basic version",
                                 formatter_class = argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('-p', dest='pmax', nargs='+', help="Maximum value for p_i of generated applications", type = float,
                    default = [1.0])
parser.add_argument('-B', nargs='+', type=int, help = "Bandwidth for writing to disk", default=[100])
parser.add_argument('-S', nargs='+', type = int, help = "Burst buffer size", default=[100])
parser.add_argument('-a', nargs='+', type=float, dest="alpha", help="Target load", default = [1.0])
parser.add_argument('--seed', nargs='+', type=int, help="random seed", default = [12345])
parser.add_argument('--no-header', dest='noheader', action='store_true', help="Do not display header at the start")
parser.add_argument('--check', action='store_true', help="Check correctness invariants")

args = parser.parse_args()

## print(args.pmax, args.B, args.S, args.alpha, args.seed, args.noheader, args.check)

if not args.noheader:
    print("seed B pmax alpha S idle")
    
for seed in args.seed:
    random.seed(seed)
    for B in args.B:
        for pmax in args.pmax:
            for alpha in args.alpha:
                l = models.generateApplis(pmax, B, alpha*B)
                d = models.distributionInstantLoad(l)
                if(args.check):
                    models.checkDistribution(d, l)
                for S in args.S:
                    P = models.transitionMatrixBasic(d, S, B)
                    if(args.check):
                        models.checkTransitionMatrix(P)
                    pi = models.getStationaryDistribution(P, S)
                    if(args.check):
                        models.checkStationaryDistribution(pi)
                    idle = models.getIdleProportionBasic(pi, S)
                    print("%d %d %g %g %d %g" % (seed, B, pmax, alpha, S, idle))




