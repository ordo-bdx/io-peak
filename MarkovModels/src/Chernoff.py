#!/usr/bin/env python3

import argparse
import models
import random

parser = argparse.ArgumentParser(description = "Distribution of Instant Load and comparison with Chernoff bounds",
                                 formatter_class = argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('-p', dest='pmax', nargs='+', help="Maximum value for p_i of generated applications", type = float,
                    default = [1.0])
parser.add_argument('-B', nargs='+', type=int, help = "Bandwidth for writing to disk", default=[100])
parser.add_argument('-a', nargs='+', type=float, dest="alpha", help="Target load", default = [1.0])
parser.add_argument('--seed', nargs='+', type=int, help="random seed", default = [12345])
parser.add_argument('--no-header', dest='noheader', action='store_true', help="Do not display header at the start")
parser.add_argument('--check', action='store_true', help="Check correctness invariants")

args = parser.parse_args()

if not args.noheader:
    print("seed B pmax alpha X AdHoc ChernoffLB ChernoffUB")
    
for seed in args.seed:
    random.seed(seed)
    for B in args.B:
        for pmax in args.pmax:
            for alpha in args.alpha:
                l = models.generateApplis(pmax, B, alpha*B)
                d = models.distributionInstantLoad(l)
                (lower, upper) = models.chernoffBounds(l)
                if(args.check):
                    models.checkDistribution(d, l)
                    models.checkChernoffBounds(d, lower, upper)
                cdf = 0
                for i in range(len(d) - 1):
                    cdf += d[i]
                    print("%d %d %g %g %d %g %g %g"
                          % (seed, B, pmax, alpha, i,
                             cdf, lower[i], upper[i]))
