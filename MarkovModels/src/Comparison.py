#!/usr/bin/env python3

from models import *
from simulator import *

import argparse
import random

parser = argparse.ArgumentParser(description = "Comparison between Markov Chain model and Discrete Event Simulator for Burst-Buffer",
                                 formatter_class = argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('-p', dest='pmax', nargs='+', help="Maximum value for p_i of generated applications", type = float,
                    default = [1.0])
parser.add_argument('-B', nargs='+', type=int, help = "Bandwidth for writing to disk", default=[100])
parser.add_argument('-S', nargs='+', type = int, help = "Burst buffer size", default=[100])
parser.add_argument('-a', nargs='+', type=float, dest="alpha", help="Target load", default = [1.0])
parser.add_argument('--var', nargs='+', type=float, help="Variability of application periods (ratio max / min)", default = [1])
parser.add_argument('--scale', nargs='+', type=float, help="Average value of application periods", default = [10])
parser.add_argument('--noise', nargs='+', type=float, help="Noise factor for the application period (period = avg +- NOISE)", default=[0.25])
parser.add_argument('-N', type=int, help="Number of runs of the discrete event simulator", default=3)
parser.add_argument('-D', type=int, help="Duration of runs of the discrete event simulator", default=5000)
parser.add_argument('--seed', nargs='+', type=int, help="random seed", default = [12345])
parser.add_argument('--no-header', dest='noheader', action='store_true', help="Do not display header at the start")
## parser.add_argument('--check', action='store_true', help="Check correctness invariants")

args = parser.parse_args()

## print(args.pmax, args.B, args.S, args.alpha, args.seed, args.noheader, args.check)

if not args.noheader:
    print("seed B pmax alpha S var scale noise duration runNB idle")
    
for seed in args.seed:
    random.seed(seed)
    for B in args.B:
        for pmax in args.pmax:
            for alpha in args.alpha:
                l = generateApplis(pmax, B, alpha*B)
                d = distributionInstantLoad(l)
                for S in args.S:
                    P = transitionMatrixBasic(d, S, B)
                    pi = getStationaryDistribution(P, S)
                    idle = getIdleProportion(pi, S, B)
                    print("%d %d %g %g %d NA NA NA NA -1 %g"
                          % (seed, B, pmax, alpha, S, idle))
                    for scale in args.scale: 
                        for var in args.var:
                            dmin = 2*scale/(1+var) # Since E(a.d) = dmin*(1+(var-1)/2)
                            for a in l:
                                a.d = dmin * (1 + random.uniform(0, var - 1))
                            for noise in args.noise: 
                                for r in range(args.N):
                                    idle = simulateDiscreteEvent(l, S, B, noise, args.D)
                                    print("%d %d %g %g %d"" %g %d %g %d %d %g"
                                          % (seed, B, pmax, alpha, S, var, scale, noise, args.D, r, idle))
                                
