#!/usr/bin/env python3

from models import *
from simulator import *

import argparse
import random

# Since Apex applications use either the full bandwidth or half the
# bandwidth to disk, the discretization of B is very easy.
B = 2

apexEAP = [PeriodicAppli(20/5700, B, 5.7)] * 13
apexLAP = [PeriodicAppli(25/12700, int(B/2), 12.7)] * 4
apexSilverton = [PeriodicAppli(280/15000, B, 15)] * 2
apexVPIC = [PeriodicAppli(23/4500, B, 4.5)] * 1

apexApplications = apexEAP + apexLAP + apexSilverton + apexVPIC
load = sum(a.p * a.b for a in apexApplications) / B


parser = argparse.ArgumentParser(description = "Comparison between Markov Chain model and Discrete Event Simulator for Burst-Buffer, on APEX applications",
                                 formatter_class = argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('-S', nargs='+', type = int, help = "Burst buffer size as percentage of bandwidth", default=[100])
parser.add_argument('-a', nargs='+', type=float, dest="alpha", help="Target load (-1 does no rescaling)", default = [-1.0])
parser.add_argument('--scale', nargs='+', type=float, help="Scale factor for application periods", default = [1])
parser.add_argument('--noise', nargs='+', type=float, help="Noise factor for the application period (period = avg +- NOISE)", default=[0.25])
parser.add_argument('-N', type=int, help="Number of runs of the discrete event simulator", default=3)
parser.add_argument('-D', type=int, help="Duration of runs of the discrete event simulator", default=5000)
parser.add_argument('--seed', nargs='+', type=int, help="random seed", default = [12345])
parser.add_argument('--no-header', dest='noheader', action='store_true', help="Do not display header at the start")
## parser.add_argument('--check', action='store_true', help="Check correctness invariants")

args = parser.parse_args()

## print(args.pmax, args.B, args.S, args.alpha, args.seed, args.noheader, args.check)

if not args.noheader:
    print("seed alpha S scale noise duration runNB idle")
    
for seed in args.seed:
    random.seed(seed)
    for alpha in args.alpha:
        l = [ a.copy() for a in apexApplications ]
        if alpha <= 0:
            stress = 1
        else:
            stress = alpha/load
        for a in l:
            a.p *= stress
            
        d = distributionInstantLoad(l)
        for Spercentage in args.S:
            S = int(B * Spercentage / 100)
            P = transitionMatrixBasic(d, S, B)
            pi = getStationaryDistribution(P, S)
            idle = getIdleProportion(pi, S, B)
            print("%d %g %d NA NA NA -1 %g"
                  % (seed, alpha, Spercentage, idle))
            for scale in args.scale:
                scaledApps = [ a.copy() for a in l ]
                for a in scaledApps:
                    a.d *= scale
                for noise in args.noise: 
                    for r in range(args.N):
                        idle = simulateDiscreteEvent(scaledApps, S, B, noise, args.D)
                        print("%d %g %d ""%d %g %d %d %g"
                              % (seed, alpha, Spercentage, scale, noise, args.D, r, idle))

