#!/usr/bin/env python3

import argparse
import random
import models
import math

parser = argparse.ArgumentParser(description = "Markov Chain model for Lazy Burst Buffer emptying",
                                 formatter_class = argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('-p', dest='pmax', nargs='+', help="Maximum value for p_i of generated applications", type = float,
                    default = [1.0])
parser.add_argument('-B', nargs='+', type=int, help = "Bandwidth for writing to disk", default=[100])
parser.add_argument('-S', nargs='+', type = int, help = "Burst buffer size", default=[100])
parser.add_argument('-a', nargs='+', type=float, dest="alpha", help="Target load", default = [1.0])
parser.add_argument('-t', nargs='+', type=float, help="Relative threshold for Lazy behavior (relative to S)", default=[-1.0, 0.5])
parser.add_argument('--mixCoef', nargs=1, type=float, help="mix coefficient needed to prove stability of the MC", default = 0.99)
parser.add_argument('--seed', nargs='+', type=int, help="random seed", default = [12345])
parser.add_argument('--no-header', dest='noheader', action='store_true', help="Do not display header at the start")
parser.add_argument('--check', action='store_true', help="Check correctness invariants")

args = parser.parse_args()

## print(args.pmax, args.B, args.S, args.alpha, args.seed, args.noheader, args.check)

if not args.noheader:
    print("seed B pmax alpha S threshold thresholdRatio keepProb idle")

args.t.sort()
    
for seed in args.seed:
    random.seed(seed)
    for B in args.B:
        for pmax in args.pmax:
            for alpha in args.alpha:
                l = models.generateApplis(pmax, B, alpha*B)
                d = models.distributionInstantLoad(l)
                if(args.check):
                    models.checkDistribution(d, l)
                for S in args.S:
                    P = models.transitionMatrixBasic(d, S, B)
                    previousThreshold = -1
                    prevPi = None
                    for t in args.t:
                        actualThreshold = -1
                        if t >= 0:
                            actualThreshold = math.floor(S * t)
                            models.updateTMforLazy(P, d, S, B, actualThreshold, previousThreshold, args.mixCoef)
                            previousThreshold = actualThreshold
                        if(args.check):
                            models.checkTransitionMatrix(P)
                        pi = models.getStationaryDistribution(P, S, start=prevPi)
                        prevPi = pi
                        if(args.check):
                            models.checkStationaryDistribution(pi)
                        idle = models.getIdleProportion(pi, S, B)
                        keepProb = models.getNoEmptyingProbability(pi, P, S)
                        print("%d %d %g %g %d %d %g %g %g" % (seed, B, pmax, alpha, S, actualThreshold, t, keepProb, idle))


