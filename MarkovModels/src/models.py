#!/usr/bin/env python3
## TODO : Licence !!


import random
import numpy as np
from numpy import linalg
import math

# Modeling an application: p_i and b_i
class Appli:
    def __init__(self, p, b):
        self.p = p
        self.b = b

    @classmethod
    def random(cls, pmax, bmax):
        return cls(random.uniform(0, pmax), random.randint(1, bmax))
    
    def load(self):
        return (self.p * self.b)
    

# Generate applications to reach a load of "target"
def generateApplis(pmax, bmax, target):
    totalLoad = 0
    result = []
    while(totalLoad < target):
        a = Appli.random(pmax, bmax)
        result.append(a)
        totalLoad += a.load()
    scale = totalLoad / target
    for a in result:
        a.p = a.p / scale
    return result

# Compute the distribution of instantaneous load of a given list of Appli
def distributionInstantLoad(applicationList):
    bValues = [a.b for a in applicationList]
    sumB = sum(bValues)
    maxB = max(bValues)
    len = sumB+maxB+1
    delta = np.zeros(len)
    delta[maxB] = 1
    for a in applicationList:
        delta[maxB:] = (1-a.p)*delta[maxB:] + a.p*delta[maxB-a.b:-a.b]
    delta = delta[maxB:]
    return delta


# Compute Chernoff bounds of the distribution of instant load
def chernoffBounds(applicationList):
    nu = sum(a.b*a.b*a.p for a in applicationList)
    e  = sum(a.b*a.p for a in applicationList)
    sumB = sum(a.b for a in applicationList)
    maxB = max(a.b for a in applicationList)
    upper = np.ones(sumB)
    lower = np.zeros(sumB)
    for k in range(math.floor(e)):
        upper[k] = math.exp(-(e-(k+1))**2 / (2*nu))
    for k in range(math.ceil(e), sumB):
        lower[k] = 1-math.exp(-((k+1)-e)**2/(2*nu+maxB*((k+1)-e)/3))
    return (lower, upper)

# Computes the transition matrix for the Markov chain
# in the basic model, with a Burst Buffer of size S and disks of
# bandwidth B
def transitionMatrixBasic(delta, S, B):
    sumB = len(delta)-1
    size = S + 1 + sumB - B
##    print(sumB,B,S,size)

    P = np.zeros( (size, size) )

    # Case when it is possible to send too little. So many
    # cases yield final buffer size = 0
    for i in range(min(B, S+1,size)):
##        print(i,min(sumB+1,B-i+1))
        P[i, 0] = sum(delta[0:min(sumB+1, B-i+1)]) # From 0 to B-i both included
##        print(i, 1,sumB-(B-i)+1,B-i+1,sumB+1)
        P[i, 1:sumB-(B-i)+1] = delta[B-i+1:sumB+1]
    
    # General case, buffer not full but cannot be emptied in one go
    for i in range(B, S+1):
        P[i, i-B:sumB+i-B+1] = delta

    for i in range(S+1, size):
        P[i, max(0, i-B)] = 1

    return P


# Update a transition Matrix to become Lazy between
# lastThreshold (excluded) and threshold (included)
def updateTMforLazy(P, delta, S, B, threshold, lastThreshold = -1, mixCoef=0.99):
    probNoMove = sum(delta[0:B+1])
    for i in range(lastThreshold+1, threshold+1): 
        P[i,i] = mixCoef * probNoMove + (1-mixCoef)*P[i,i]
        P[i, 0:i] *= (1-mixCoef)
    return P

# Computes the stationary Distribution of a Matrix P
# By iterative refinements
def getStationaryDistribution(P, S=-1, start=None):
    if(P.shape[0] != P.shape[1]):
        print("getStationaryDistribution: non square matrix: shape= (%d, %d)"
              % (P.shape[0], P.shape[1]))
        exit
    size = P.shape[0]
    if not start is None:
        prStat = start
    else:
        if S > 0:
            prStat = np.zeros(size)
            prStat[0:S] = 1
        else:
            prStat = np.ones(size)
        prStat = prStat / sum(prStat)
    change = 1
    iterations = 0
    while change > 1e-6:
        prStatNew = np.dot(prStat, P) ## TODO Check.
        prStatNew = prStatNew/sum(prStatNew)
        change = linalg.norm(prStatNew - prStat)
        prStat = prStatNew
        iterations += 1
    return prStat



# Computes the proportion of idle time in the stationary distribution
# Considering that empyting the buffer always costs 1, even
# if not enough data is present
def getIdleProportionBasic(prStat, S):
    return sum(prStat[S+1:])

# Computes the proportion of idle time considering that emptying half
# of the buffer takes hale a unit of time (only changes something
# if S < B)
def getIdleProportion(prStat, S, B):
    if(S < B):
        partialEmptying = sum(prStat[S+1:B+1] * np.arange(S+1, B+1)) / B
        fullEmptying = sum(prStat[B+1:])
        return ( (fullEmptying + partialEmptying)
                 / (fullEmptying + partialEmptying + sum(prStat[:S+1])) )
    else: 
        return sum(prStat[S+1:])

# Compute the probability that the BB is not emptied
def getNoEmptyingProbability(prStat, P, S):
    result = 0
    for i in range(S+1):
        result += prStat[i] * sum(P[i,i:])
    return result

    
# Code for testing some invariants. Checked with some tolerance to
# floating-point errors
def isDifferent(x, y):
    return abs(x-y) / max(x,y) > 1e-6

def checkDistribution(dist, applicationList):
    if isDifferent(sum(dist), 1):
        print("Total distribution sum: %g" % sum(dist))
        return False
    pVector = np.array([a.p for a in applicationList])
    if isDifferent(dist[0], np.prod(1-pVector)):
        print("Probability of 0 load: %g vs Product of 1-p_i: %g" % (dist[0], np.prod(1-pVector)))
        return False
    if isDifferent(dist[-1], np.prod(pVector)):
        print("Probability of max load: %g vs Product of p_i: %g" % (dist[-1], np.prod(pVector)))
        return False
    return True

def checkChernoffBounds(dist, lower, upper):
    cdf = 0
    for i in range(len(dist)-1):
        cdf += dist[i]
        if(cdf < lower[i] or cdf > upper[i]):
            print("Wrong Chernoff bound at index %d: values should satisfy %g < %g < %g"
                  % (i, lower[i], dist[i], upper[i]))
            return False
    return True

def checkTransitionMatrix(P):
    for i in range(P.shape[0]):
        if(isDifferent(sum(P[i,:]), 1)):
            print("Transition Matrix row %d: sum of entries is not 1: %g"
                  % (i, sum(P[i,:])))
            return False
    return True

def checkStationaryDistribution(prStat):
    if(isDifferent(sum(prStat), 1)):
        print("Stationary distribution: sum of entries is not one: %g"
              % sum(prStat))
        return False
    return True
       
# Some basic tests can be run here.
if __name__ == "__main__":
    l = generateApplis(0.2, 100, 100)
    d = distributionInstantLoad(l)
    if(not checkDistribution(d, l)):
        exit(-1)
    P = transitionMatrixBasic(d, 200, 100)
    if(not checkTransitionMatrix(P)):
        exit(-1)
    updateTMforLazy(P, d, 200, 100, 60)
    if(not checkTransitionMatrix(P)):
        exit(-1)
    updateTMforLazy(P, d, 200, 100, 120, 60)
    if(not checkTransitionMatrix(P)):
        exit(-1)
    Q = transitionMatrixBasic(d, 200, 100)
    updateTMforLazy(Q, d, 200, 100, 120)
    if(not np.allclose(P, Q)):
        print("Lazy in two steps not equivalent to Lazy in one step")
        exit(-1)

    print("Test OK !")
    exit(0)
        
    
if(False):
    l = [m.Appli(0.3, 4), m.Appli(0.5, 3)]
    d = m.distributionInstantLoad(l)
    P = m.transitionMatrixBasic(d, 2, 3)
    l[0].d = 6
    l[1].d = 10

# Equivalent Scilab code:
# p = [0.3; 0.5];
# b = [4; 3];
# d = [6; 10]; 
# NbApps = 2;
# B = 3;
# S = 2;
        
