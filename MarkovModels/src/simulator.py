#!/usr/bin/env python3


import random
import heapq as hq
import math
from models import Appli

StartEmission = 'start'
StopEmission  = 'stop'

class PeriodicAppli(Appli):
    def __init__(self, p, b, d):
        Appli.__init__(self, p, b)
        self.d = d
    def copy(self):
        return PeriodicAppli(self.p, self.b, self.d)


def simulateDiscreteEvent(applicationList, S, B, noiseFactor, maxDuration):

    # Initialize state
    events = []
    tNow = 0
    wastedTime = 0
    fillSpeed = -B
    bufferLoad = 0 # should remain below S at all time.
    saturationTime = math.inf
    
    def updateSatTime():
        nonlocal saturationTime
        if fillSpeed > 0: 
            saturationTime = tNow + (S - bufferLoad)/fillSpeed
        else:
            saturationTime = math.inf
        
    def startEmission(a):
        nonlocal fillSpeed, events
        fillSpeed += a.b
        duration = (1 + noiseFactor*random.uniform(-1, 1)) * a.p * a.d
        hq.heappush(events, [tNow+duration, a.id, StopEmission] )
        updateSatTime()
        
    def stopEmission(a):
        nonlocal fillSpeed, events
        fillSpeed -= a.b
        duration = (1 + noiseFactor*random.uniform(-1, 1)) * (1-a.p) * a.d
        hq.heappush(events, [tNow+duration, a.id, StartEmission] )
        updateSatTime()

    id = 0
    for a in applicationList:
        a.id = id
        id += 1
        offset = random.uniform(0, a.d)
        hq.heappush(events, [offset, a.id, StartEmission] )

    finished = False
    while not finished:

        time, app, evType = events[0]

##        print("%g %g %g %g \t\t\t   %d %g (%g) %s" % (tNow, bufferLoad, fillSpeed, wastedTime, app, time, saturationTime, evType))
        
        if time <= saturationTime:
            # Actually consume the event
            hq.heappop(events)
            bufferLoad = max(0, bufferLoad + fillSpeed*(time - tNow))
            tNow = time
            if time > maxDuration:
                finished = True
                break
            if evType is StartEmission:
                startEmission(applicationList[app])
            else:
                stopEmission(applicationList[app])
        else:
            pauseLength = min(1, S/B)
            tNow = saturationTime + pauseLength
            wastedTime += pauseLength
            for ev in events:
                ev[0] += pauseLength
            bufferLoad = max(0, S-B)
            updateSatTime()

        
    return (wastedTime/tNow)

