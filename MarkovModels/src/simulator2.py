#!/usr/bin/env python3


import random
import heapq as hq
import math

StartEmission = 'start'
StopEmission  = 'stop'

def simulateDiscreteEvent(applicationList, S, B, noiseFactor, maxDuration):

    # Initialize state
    events = []
    nbApps = len(applicationList)
    nbTasks = [0] * nbApps
    nbTasksExpected = [ maxDuration/a.d for a in applicationList ]
    tNow = 0
    wastedTime = 0
    fillSpeed = -B
    bufferLoad = 0 # should remain below S at all time.
    saturationTime = math.inf
    
    def updateSatTime():
        nonlocal saturationTime
        if fillSpeed > 0: 
            saturationTime = tNow + (S - bufferLoad)/fillSpeed
        else:
            saturationTime = math.inf
        
    def startEmission(a):
        nonlocal fillSpeed, events
        fillSpeed += a.b
        duration = (1 + noiseFactor*random.uniform(-1, 1)) * a.p * a.d
        hq.heappush(events, [tNow+duration, a.id, StopEmission] )
        updateSatTime()
        
    def stopEmission(a):
        nonlocal fillSpeed, events
        fillSpeed -= a.b
        duration = (1 + noiseFactor*random.uniform(-1, 1)) * (1-a.p) * a.d
        hq.heappush(events, [tNow+duration, a.id, StartEmission] )
        nbTasks[a.id]+=1
        updateSatTime()

    id = 0
    for a in applicationList:
        a.id = id
        id += 1
        offset = random.uniform(0, a.d)
        hq.heappush(events, [offset, a.id, StartEmission] )

    finished = False
    while not finished:

        time, app, evType = events[0]

##         print("%g (%g) %g %d %s" % (time, saturationTime, fillSpeed, app, evType))
        
        if time <= saturationTime:
            # Actually consume the event
            hq.heappop(events)
            bufferLoad = max(0, bufferLoad + fillSpeed*(time - tNow))
            tNow = time
            if time > maxDuration:
                finished = True
                break
            if evType is StartEmission:
                startEmission(applicationList[app])
            else:
                stopEmission(applicationList[app])
        else:
            time = saturationTime
            bufferLoad = B

            rho = B / (fillSpeed+B)
            nextEvent = min( ev[0] if ev[2] is StartEmission else time + (1/rho)*(ev[0] - time)
                          for ev in events )
            for ev in events:
                if ev[2] is StopEmission:
                    ev[0] += (nextEvent - time)*(1-rho)
            tNow = nextEvent
            saturationTime = math.inf


    stretch = sum(nbTasks[i] / nbTasksExpected[i] for i in range(nbApps) )
    return (1 - stretch/nbApps)

