#!/usr/bin/env python3
  
import random
import heapq as hq
import math
import numpy as np
import matplotlib.pyplot as plt
from models import *
from simulator import *




    
class PeriodicAppli(Appli):
    def __init__(self, p, b, d):
        self.p = p
        self.b = b
        self.d = d
        
    @classmethod
    def copy(self):
        return PeriodicAppli(self.p, self.b, self.d)
    def random(cls, pmax, bmax, dmax):
        return cls(random.uniform(0, pmax), random.randint(1, bmax), random.randint(1, dmax))

    def generateDistrib(self,tcar):
        a=self
        instantloadApp=[0]*(2*self.d)
        instantloadApp[0:math.floor(self.d*(1-a.p))]=np.zeros(math.floor(a.d*(1-a.p)))
        instantloadApp[math.floor(a.d*(1-a.p)):math.floor(a.d)]=a.b*np.ones(math.floor(a.d*a.p))
        instantloadApp[math.floor(a.d):math.floor(a.d+a.d*(1-a.p))]=np.zeros(math.floor(a.d*(1-a.p)))
        instantloadApp[math.floor(a.d)+math.floor(a.d*(1-a.p)):math.floor(a.d)+math.floor(a.d)]=a.b*np.ones(math.floor(a.d*a.p))

        minimumload=math.floor(tcar/a.d)*a.b*a.p*a.d
        instantloadIntegral=[minimumload]*(a.d)

        
        for x in range(0,a.d):
            for delta in range(x,x+ tcar-math.floor(tcar/a.d)*a.d):
                instantloadIntegral[x]+=instantloadApp[delta]

##        print('InstantLoadApp',instantloadApp)
##        print('InstantLoadIntegral',instantloadIntegral)

        compte={}.fromkeys(set(instantloadIntegral),0)
        for valeur in instantloadIntegral:
            compte[valeur]+=1/a.d

##        for cle in compte.keys():
##            print(cle,compte.get(cle))
##    
        return(compte)




def ExpectedInstantLoad(appliList, tcar):
    summaxkey=0
    maxkey=0
    for a in appliList:
        x=a.generateDistrib(tcar)
        summaxkey+=max(x.keys())
        maxkey=max(maxkey,max(x.keys()))
    sizedelta=math.floor(maxkey+summaxkey+1)
    maxkey=math.floor(maxkey)
    delta = np.zeros(sizedelta)
    delta[maxkey]=1
 
    for a in appliList:
        delta2 = np.zeros(sizedelta)
        x=a.generateDistrib(tcar)
        for cle in x.keys():
            delta2[maxkey:sizedelta]+=x.get(cle)*delta[maxkey-math.floor(cle):sizedelta-math.floor(cle)]
        delta=delta2
    delta=delta[maxkey:]
##    fig = plt.figure()
##    plt.plot(delta2)
##    plt.show()
    return(delta)


def EstimateWastedMarkov(appliList, tcar,B,S):
    deltaOcc=ExpectedInstantLoad(appliList, tcar)
    POcc=transitionMatrixBasic(deltaOcc, S, tcar*B)
    prstatOcc=getStationaryDistribution(POcc)
    idleOcc=getIdleProportionBasic(prstatOcc, S)
    return(idleOcc)



def generateApplis(pmax, bmax, dmax, target):
    totalLoad = 0
    result = []
    while(totalLoad < target):
        a = PeriodicAppli(random.uniform(0, pmax), random.randint(1, bmax), random.randint(1, dmax))
        result.append(a)
        totalLoad += a.p*a.b
    scale = totalLoad / target
    for a in result:
        a.p = a.p / scale
        print(a.p,a.b,a.d)
    return result


def tryAllCharacteristicTimes(listApplic, S, B, tcarmax):

    wasted=np.zeros(tcarmax)
    idleMarkov=np.zeros(tcarmax)

    for tcar in range (1,tcarmax):
        wasted[tcar]=simulateDiscreteEvent(listApplic, S, B, noiseFactor, maxDuration)
        idleMarkov[tcar]=EstimateWastedMarkov(listApplic, tcar,B,S)
        print(tcar,wasted[tcar],idleMarkov[tcar])

    fig = plt.figure()
    plt.plot(idleMarkov[1:tcarmax])
    plt.plot(np.mean(wasted[1:tcar])*np.ones(tcarmax-1))
    plt.plot(np.min(wasted[1:tcar])*np.ones(tcarmax-1))
    plt.plot(np.max(wasted[1:tcar])*np.ones(tcarmax-1))
    plt.show()



def essai():

    aa=PeriodicAppli(0.2, 500, 30)
    bb=PeriodicAppli(0.5, 100, 2)
    applicList=[]
    averageLoad=0

    for i in range(3):
        applicList.append(aa)
        averageLoad+=aa.b*aa.p
        applicList.append(bb)
        averageLoad+=bb.b*bb.p


    B=math.floor(1.1*averageLoad)
    S=math.floor(1.1*B)
    tcarmax=20
    tryAllCharacteristicTimes(applicList, S, B, tcarmax)








B=100
target=0.95
pmax=0.5
dmax=30
tcarmax=30
S=math.floor(1.1*B)
noiseFactor=0.0
maxDuration=5000




listAppli=generateApplis(pmax, B, dmax, target*B)
lload=0
for a in listAppli:
    print(a.p,a.b,a.d)
    lload+= a.p*a.b
print(lload)
tryAllCharacteristicTimes(listAppli, S, B, tcarmax)
#essai()







