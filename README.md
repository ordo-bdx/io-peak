
This repository contains code used to evaluate two papers: 

+ "**What Size Should your Buffers to Disks be?**" by Aupy, Beaumont,
  Eyraud-Dubois presented at the conference IPDPS'18
+ "**Sizing and Partitioning Strategies for Burst-Buffers to Reduce IO
Contention**" by Aupy, Beaumont, Eyraud-Dubois, submitted to the
conference IPDPS'19.

# What Size Should your Buffers to Disks be?

We provide in directory `MarkovModels` the code used to evaluate the
paper: "*What Size Should your Buffers to Disks be?*" by Aupy,
Beaumont, Eyraud-Dubois presented at the conference IPDPS'18.

Specifically, the code corresponding to
[version 1.0](https://gitlab.inria.fr/ordo-bdx/io-peak/tags/v1.0) is
the version used to generate the figures in the paper. Use the
*Download* button to obtain the code.

All figures can be generated with the `Makefile` available in
directory `MarkovModels`, and they are produced in directory
`plots`. **Just type `make`**. Specific targets are provided for each
individual figure in the article:

+ Target `Chernoff` produces `Chernoff.pdf` which corresponds to
*Figure 2*. It also produces `ChernoffAll.pdf` and
`AdHocVariability.pdf` which are not part of the paper, but are used
to analyze the dependency of the result on the random seed.
+ Target `Basic` produces `Basic.pdf` which corresponds to *Figure 3*.
+ Target `Comparison` produces `Comparison.pdf` which corresponds to *Figure 4*.
+ Target `ComparisonApex` produces `ComparisonApex.pdf` which corresponds to *Figure 5*.
+ Target `Lazy` produces `Lazy.Waste.pdf` and `Lazy.Keep.pdf` which
  correspond respectively to *Figure 6* and *Figure 7*.

## Directory structure

The subdirectory `src` contains the Python code that performs the
simulations depicted in the paper. The file `models.py` performs the
Markov Chain modelisation, and `simulator.py` contains the code of the
discrete event simulator. The other Python files correspond to the
figures described above, and have informative `--help` messages.

The subdirectory `data` contains the raw data produced by this Python
code. Parameter values used in the paper are set in the Bash scripts
available in the main directory. The subdirectory `analysis` contains
R code used to generate plots, which are written to subdirectory
`plots`.


# Sizing and Partitioning Strategies for Burst-Buffers to Reduce IO Contention

We provide in directory `LinearModels` the code used to evaluate the
paper: "*Sizing and Partitioning Strategies for Burst-Buffers to
Reduce IO Contention*" by Aupy, Beaumont, Eyraud-Dubois, submitted to
the conference IPDPS'19.

Specifically, the code corresponding to
[version 2.0](https://gitlab.inria.fr/ordo-bdx/io-peak/tags/v2.0) is
the version used to generate the figures in the paper. Use the
*Download* button to obtain the code.

All figures can be generated with the `Makefile` available in
directory `LinearModels`, and they are produced in directory
`plots`. **Just type `make`**. This will ultimately produce
`plots/intrepid-maxstretch.pdf` and `plots/intrepid-avgstretch.pdf`,
which correspond respectively to the top and bottom plots of *Figure
6*. The output of the last script also provides the contents of the
*table at the bottom of Page 9*. 

## Dependencies

This code requires Python 3, and the linear programming formulations
depend on [PuLP](https://pythonhosted.org/PuLP/) which can be
installed with `pip install pulp`. To solve the formulations
sufficiently fast, it is advised to use IBM Cplex by pointing to it in
the `Makefile`. Otherwise the default `PuLP` solver is used. `PuLP`
allows to use other solvers (see their
[documentation page](https://pythonhosted.org/PuLP/solvers.html) about
this) if preferred, but this would require to modify `algLP.py` around
line 79.

## Directory structure

The subdirectory `src` contains the Python code that performs the
simulations depicted in the paper. The file `instance.py`contains
basic handling of input data and computing of the time points of each
application and the amount of data transferred at these time
points. The file `algLP.py` implements the linear formulations of the
paper. The file `greedy.py` implements the greedy strategy described
in Section VII-a. The file `intrepid.py` contains data from the
Intrepid platform and drives the whole experiment. 

The subdirectory `data` contains the raw data produced by this Python
code. The subdirectory `analysis` contains R code used to generate
plots, which are written to subdirectory `plots`.


# Licence information:

This software is governed by the CeCILL 2.1 license under French law
and abiding by the rules of distribution of free software.  You can
use, modify and/ or redistribute the software under the terms of the
CeCILL license available in the file "LICENSE".

Copyright Inria

*Contributors:* Lionel Eyraud-Dubois, Olivier Beaumont, Guillaume Aupy
(2018)

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
